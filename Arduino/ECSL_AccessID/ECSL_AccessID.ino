#include <SPI.h>
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "src/rfid/VirtualSPI.h"
#include "src/rfid/MFRC522.h"
#include "src/I32CTT/I32CTT.h"
#include "src/I32CTT/I32CTT_Arduino802154Interface.h"
#include "src/I32CTT/I32CTT_NullEndpoint.h"

#define PIN  17
#define NUMPIXELS      1
Adafruit_NeoPixel pixel = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

const uint8_t SCK0_PIN = 14;
constexpr uint8_t RST_PIN = 19; // Configurable, see typical pin layout above
constexpr uint8_t SS_PIN = 10; // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance

I32CTT_Controller controller(1);
I32CTT_Arduino802154Interface ieee802154;
I32CTT_NullEndpoint idleEndpoint(I32CTT_Endpoint::str2id("NUL"));

uint8_t device_mode = 1;

unsigned long last_read = 0;
unsigned long t_elapsed = 0;
uint16_t pan_id = 0;
uint16_t short_addr = 0;
uint16_t gateway_addr = 0;

void setup() {
  SPI1.begin();
  HardwareSPI.begin();      // Init SPI bus
  SPI.setSCK(SCK0_PIN);

  // Configure pin 13
  CORE_PIN13_CONFIG = PORT_PCR_MUX(1);
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  // and then reassign pin 14 to SCK
  CORE_PIN14_CONFIG = PORT_PCR_DSE | PORT_PCR_MUX(2);
  
	Serial.begin(9600);		// Initialize serial communications with the PC
	//while (!Serial);		// Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)

  // Read configuration from memory
  pan_id = EEPROM.read(0);
  pan_id |= ((uint16_t)EEPROM.read(1))<<8;
  
  short_addr = EEPROM.read(2);
  short_addr |= ((uint16_t)EEPROM.read(3))<<8;
  
  gateway_addr = EEPROM.read(4);
  gateway_addr |= ((uint16_t)EEPROM.read(5))<<8;
  
  device_mode = EEPROM.read(6);

  pixel.begin();

  Serial.println(F("Serial available..."));

  pinMode(RST_PIN, OUTPUT);
  digitalWrite(RST_PIN, HIGH);
  pinMode(SS_PIN, OUTPUT);
  digitalWrite(SS_PIN, HIGH);

  ieee802154.set_SPI(SPI1);

  ieee802154.cs_pin = 6;
  pinMode(ieee802154.cs_pin, OUTPUT);
  digitalWrite(ieee802154.cs_pin, HIGH);
  ieee802154.slp_tx_pin = 5;
  pinMode(ieee802154.slp_tx_pin, OUTPUT);
  digitalWrite(ieee802154.slp_tx_pin, LOW);
  ieee802154.rst_pin = 4;
  pinMode(ieee802154.rst_pin, OUTPUT);
  digitalWrite(ieee802154.rst_pin, LOW);
  ieee802154.irq_pin = 7;

  Serial.println(F("Serial available..."));

  mfrc522.PCD_Init();   // Init MFRC522
  mfrc522.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details

  Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));

  ieee802154.set_pan_id(pan_id);
  ieee802154.set_short_addr(short_addr);
  ieee802154.set_channel(C2480);
  ieee802154.set_dst_addr(gateway_addr);

  controller.set_interface(ieee802154);
  controller.add_mode_driver(idleEndpoint);
  controller.enable_scheduler();
  controller.init();

  last_read = millis();
  t_elapsed = millis();
}

uint32_t string_to_integer(uint8_t *ptr) {
  uint32_t data;
  data = ptr[0];
  data |= ptr[1]<<8;
  data |= ptr[2]<<16;
  data |= ptr[3]<<24;
  return data;
}

uint8_t next_color[3] = {0,0,0}; 
uint8_t led_state = 0;


long first_card_evt = 0;
uint8_t reader_state = 0;

uint32_t card1, card2;

int pulse_delay = 2000;

#define BUFF_SIZE 25
char input_buffer[BUFF_SIZE];
uint8_t buf_pos = 0;

void flush_buffer() {
  for(int i = 0;i < BUFF_SIZE;i++) {
    input_buffer[i] = 0;
  }
  buf_pos = 0;
}

void append_buffer(char c) {
  if(buf_pos<(BUFF_SIZE-1)) {
    input_buffer[buf_pos++] = c; 
  } else {
    flush_buffer();
  }
}

void process_command(char *buff) {
  uint32_t data;
  //Serial.print("Processing command");
  if(strstr(buff,"set_pan_id")) {
    char *pch = strtok(buff,":");
    if(pch==NULL) {
      return;
    }
    pch = strtok(NULL, ":");
    if(pch!=NULL) { // Found our token
      data = strtol(pch, NULL, 16);
      EEPROM.write(0,(uint8_t)(0xFF&data));
      EEPROM.write(1,(uint8_t)(data>>8));
      pan_id = (uint16_t)(0xFFFF&data);
      ieee802154.set_pan_id(pan_id);
      Serial.println(F("CMD_OK"));
    }
  }
  if(strstr(buff,"set_short_addr")) {
    char *pch = strtok(buff,":");
    if(pch==NULL) {
      return;
    }
    pch = strtok(NULL, ":");
    if(pch!=NULL) { // Found our token
      data = strtol(pch, NULL, 16);
      EEPROM.write(2,(uint8_t)(0xFF&data));
      EEPROM.write(3,(uint8_t)(data>>8));
      short_addr = (uint16_t)(0xFFFF&data);
      ieee802154.set_short_addr(short_addr);
      Serial.println(F("CMD_OK"));
    }
  }
  if(strstr(buff,"set_gateway_addr")) {
    char *pch = strtok(buff,":");
    if(pch==NULL) {
      return;
    }
    pch = strtok(NULL, ":");
    if(pch!=NULL) { // Found our token
      data = strtol(pch, NULL, 16);
      EEPROM.write(4,(uint8_t)(0xFF&data));
      EEPROM.write(5,(uint8_t)(data>>8));
      gateway_addr = (uint16_t)(0xFFFF&data);
      ieee802154.set_dst_addr(gateway_addr);
      Serial.println(F("CMD_OK"));
    }
  }
  if(strstr(buff,"set_mode")) {
    char *pch = strtok(buff,":");
    if(pch==NULL) {
      return;
    }
    pch = strtok(NULL, ":");
    if(pch!=NULL) { // Found our token
      data = strtol(pch, NULL, 16);
      EEPROM.write(6,(uint8_t)(0xFF&data));
      device_mode = (uint16_t)(0xFF&data);
      Serial.println(F("CMD_OK"));
    }
  }
}

void loop() {

  if(Serial.available()) {
    char c = Serial.read();
    //Serial.write(c);
    append_buffer(c);
    if(c == '\n' || c == '\r') {
      process_command(input_buffer);
      flush_buffer();
    }
  }

  
  bool card_ready = true;
  //pinMode(RST_PIN, OUTPUT);
  //digitalWrite(RST_PIN, LOW);
  controller.run();
  
  if(device_mode == 1 && reader_state == 1) {
    if((millis()-first_card_evt) > 10000) {
      reader_state = 0;
      pulse_delay = 2000;
      tone(18, 1500, 500);
    }
  }
  
  //mfrc522.PCD_Init();  
	if ( ! mfrc522.PICC_IsNewCardPresent()) {
		card_ready &= false;
	}

	if ( ! mfrc522.PICC_ReadCardSerial()) {
		card_ready &= false;
	}
  //pinMode(RST_PIN, OUTPUT);
  //digitalWrite(RST_PIN, LOW);

  
  if(card_ready&&((millis()-last_read)>1000)) {
    
    uint32_t data = 0x1234ABCD; // Extract the first 4 octets
    
    Serial.print("Sending...");
    if(mfrc522.uid.size>=4) {
      data = string_to_integer(mfrc522.uid.uidByte);
    }
    switch(device_mode) {
      case 0: // Record visit
        controller.master.set_mode(0);
        Serial.println("Mode set");
        controller.master.write_record({0, data});
        Serial.println("Record built");
        controller.master.try_send();
        tone(18, 2050, 125);
        break;
      case 1: // Sharepoint mode
        switch(reader_state) {
          case 0: // first card received
            first_card_evt = millis();
            card1 = data;
            reader_state = 1;
            tone(18, 2050, 125);
            pulse_delay = 500;
            break;
          case 1:
            if(card1 != data) {
              // Two different cards received
              // Send data to master
              controller.master.set_mode(1);
              controller.master.write_record({0, card1});
              controller.master.write_record({1, data});
              controller.master.try_send();
              pulse_delay = 2000;
              reader_state = 0;
              tone(18, 2050, 125);
            }
            break;
        }
        break;
      case 2: // Card-init mode
        controller.master.set_mode(2);
        //Serial.println("Mode set");
        controller.master.write_record({0, data});
        //Serial.println("Record built");
        controller.master.try_send();
        tone(18, 2050, 125);
        break;
      case 3: // Almuerzo
        controller.master.set_mode(3);
        //Serial.println("Mode set");
        controller.master.write_record({0, data});
        //Serial.println("Record built");
        controller.master.try_send();
        tone(18, 2050, 125);
        break;
    }
    
    Serial.println("Sent.");
    last_read = millis();
  }

  switch(led_state) {
    case 0:
      if((millis()-t_elapsed)>pulse_delay) {
        switch(reader_state) {
          case 0:
            if(ieee802154.get_link_health()==0) {
              next_color[0] = 0x55;
              next_color[1] = 0x00;
              next_color[2] = 0x00;
            } else {
              next_color[0] = 0x00;
              next_color[1] = 0x55;
              next_color[2] = 0x00;
            }
            break;
          case 1:
            next_color[0] = 0x55;
            next_color[1] = 0x55;
            next_color[2] = 0x00;
            break;
        }
        t_elapsed = millis();
        led_state = 1;
        pixel.setPixelColor(0, pixel.Color(next_color[0],next_color[1],next_color[2]));
        pixel.show();
      }
      break;
    case 1:
      if((millis()-t_elapsed)>25) {
        t_elapsed = millis();
        next_color[0] = 0x00;
        next_color[1] = 0x00;
        next_color[2] = 0x00;
        led_state = 0;
        pixel.setPixelColor(0, pixel.Color(next_color[0],next_color[1],next_color[2]));
        pixel.show();
      }
      break;
  }
}
