# Bienvenido a EasyAccess
Easy Access es la plataforma IoT para realizar el control de registro, asistencia a charlas y rastreo de participantes del [X Encuentro Centroaméricano de Software Libre desarrollado el 2018 en El Salvador](http://ecsl2018.softwarelibre.ca/).

La plataforma EasyAccess es completamente Open-Source Hardware y Software por lo que puede ser replicada fácilmente y extendida para soportar todo tipo de eventos y escenarios de uso.

## Funcionamiento
Easy Access utiliza lectores inalámbricos NFC para generar "eventos" al entrar en contacto con las tarjetas de los participantes. Estos eventos son transmitidos de forma inalámbrica al "Fog-Controller" donde son decodificados, procesados y almacenados localmente. Esto permite que el sistema pueda seguir funcionando aún si la conexión a Internet es inestable.

Una vez es detectada una conexión a Internet los eventos son transmitidos a través de [llamadas de API](./docs/Endpoints_API.md) a la plataforma de gestión del X ECSL para que puedan activarse o asociarse las acciones correspondientes.

### Estado de desarrollo del proyecto
Este proyecto se encuentra en fase Alpha, es importante considerar que por el momento no se están utilizando provisiones de seguridad para la comunicación entre nodos y el gateway por lo que no se recomienda utilizarlo en entornos que requieran algún tipo de garantía de autenticidad del origen.

## Componentes del Sistema
Para su funcionamiento EasyAccess está compuesto de dos elementos:

 1. Fog-Controller IoT (Gateway) con Raspberry Pi
 2. Lectores inalámbricos NFC elaborados con Arduino

Los lectores NFC utilizan el estándar IEEE 802.15.4 para comunicarse de manera inalámbrica con el Gateway. En la versión utilizada en el X ECSL únicamente se utiliza el id incluído en la tarjeta y no se guardan datos en las mismas.

## Fog-Controller IoT 802.15.4
El Fog-Controller ha sido implementado utilizando una Raspberry Pi 3 y el Radio 802.15.4 ATZB-RF233 de Microchip. El Fog-Controller implementa la siguiente funcionalidad:

 1. Expone una Plataforma de Gestión Local para administración de dispositivos.
 2. Escucha por mensajes transmitidos a través de 802.15.4
 3. Sincroniza los datos locales con la plataforma en la nube a través de las [llamadas de API](./docs/Endpoints_API.md) (cuando está disponible el acceso a Internet)

## Lectores NFC Inalámbricos
Los lectores NFC inalámbricos utilizan los lectores de tarjetas NFC de bajo costo MFRC522, las tarjetas electrónicas Teensy LC y los radios 802.15.4 de openlabs.co. El detalle de los componentes y sus costos se encuentran en la carpeta **hardware**.

Los lectores interactúan con el Fog-Controller para proveer de la siguiente funcionalidad:

 1. Asociar participantes a tarjetas en el sistema.
 2. Enviar eventos de asistencia.
 3. Enviar eventos de "almuerzo"
 4. Enviar eventos de compartir-información (Sharepoint)

### Protocolo de comunicación I32CTT
Esta implementación utiliza el protocolo de intercambio de mensajes de telemetría y control desarrollado por el Hackerspace San Salvador denominado "I32CTT". Este protocolo permite enviar mensajes de forma fácil sobre redes de configuración puntu-multipunto. Más información sobre I32CTT puede encontrarse en el [repositorio oficial](https://github.com/hackerspacesv/I32CTT).

## Contenido de este repositorio
Este repositorio incluye las siguientes carpetas:

 1. **web**: Incluye el código fuente de la plataforma de gestión local escrito en CakePHP 3.
 2. **hardware**: Diseño de las tarjetas electrónicas de los lectores y lista de materiales para su construcción.
 3. **modules**: Sub-módulos (dependencias) del repositorio.
 4. **python**: Scripts de python de configuración de nodos, servicio de escucha de eventos sobre 802.15.4 y sincronización con el servidor.
 5. **docs**: Archivos de documentación (API, instalación, configuración, etc).
 6. **sql**: Archivos SQL de inicialización de la base en SQLite3.

## Licencia de uso
Copyright 2018 [Hackerspace San Salvador](http://hackerspace.teubi.co/).

El software y archivos de diseño de este repositorio se distribuyen "tal cual" con la expectativa que sean útiles para terceros. Sin embargo, los autores no pueden garantizar su funcionamiento bajo ninguna circunstancia.

Todos los archivos de código fuente incluidos como parte de este repositorio se distribuyen bajo licencia [GNU-GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) a menos que se especifique lo contrario.

Todos los archivos de diseño, documentación, artes gráficos y demás que no correspondan a marcas registradas de terceros se distribuyen bajo la licencia [Creative Commons BY-SA v 4.0](https://creativecommons.org/licenses/by-sa/4.0/).