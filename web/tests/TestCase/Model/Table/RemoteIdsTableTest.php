<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RemoteIdsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RemoteIdsTable Test Case
 */
class RemoteIdsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RemoteIdsTable
     */
    public $RemoteIds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.remote_ids',
        'app.users',
        'app.remotes',
        'app.cards'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RemoteIds') ? [] : ['className' => RemoteIdsTable::class];
        $this->RemoteIds = TableRegistry::getTableLocator()->get('RemoteIds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RemoteIds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
