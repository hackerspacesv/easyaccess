<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ApiController extends AppController
{
  public function initialize()
  {
      parent::initialize();
      $this->Auth->allow(['getUsers']);
  }

  /*
   * This function returns test data to import to the system.
   */
  public function getUsers() {

    $data = array(
      array(
        'id' => 1,
        'class' => 1,
        'name' => 'Mario Gómez'
      ),
      array(
        'id' => 2,
        'class' => 2,
        'name' => 'Carlos Hernández'
      ),
      array(
        'id' => 3,
        'class' => 1,
        'name' => 'Juan Juarez'
      ),
    );

    $data_json = json_encode($data);

    $this->response->type('json');
    $this->response->body($data_json);
    return $this->response;
  }

  public function getConfig() {
    $user_id = $this->Auth->user('id');
        
    $users = TableRegistry::getTableLocator()->get('Users');
    $user = $users->findById($user_id)->firstOrFail();

    $data_json = json_encode($user);

    $this->response->type('json');
    $this->response->body($data_json);
    return $this->response;    
  }

  public function getDevices() {
    $user_id = $this->Auth->user('id');

    $devices = TableRegistry::getTableLocator()->get('Devices');
    $devices->belongsTo('DeviceFeatures');
    $deviceFeatures = TableRegistry::getTableLocator()->get('DeviceFeatures');
    $user_devices = $devices->find()
                    ->select(['id','user_id','name','short_address','device_feature_id'])
                    ->select($devices->DeviceFeatures)
                    ->where(['user_id' =>$user_id])
                    ->contain('DeviceFeatures');

    $data_json = json_encode($user_devices->toArray());

    $this->response->type('json');
    $this->response->body($data_json);
    return $this->response;
  }

  public function isAuthorized($user) {
    return true;
  }

}
