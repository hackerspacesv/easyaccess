<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ManageController extends AppController
{

  public function initialize()
  {
    parent::initialize();

    $this->loadComponent('Paginator');
    $this->loadComponent('Flash');
  }

  public function index() {
  }

  public function config() {
    $user_id = $this->Auth->user('id');

    $users = TableRegistry::getTableLocator()->get('Users');
    $user = $users->findById($user_id)->firstOrFail();
    if ($this->request->is(['post','put'])) {
      $data = $this->request->getData();

      $error_msg = "";

      $valid = True;

      // Validate all conditions that we cannot validate
      // using the data validation premises
      // 1: Check if passwords match.
      if(isset($data['password']) &&
          isset($data['password_rpt']) &&
          $data['password'] !== ""
        ) {
        if($data['password']!==$data['password_rpt'])  {
          $error_msg .= "Las claves no coinciden.";
          $valid &= False;
        } else {
          // Remove additional field
          unset($data['password_rpt']);
        }
      } else {
        // One or two fields are missing
        // remove both to prevent them to erase current password content
        if(isset($data['password'])) {
          unset($data['password']);
        }
        if(isset($data['password_rpt'])) {
          unset($data['password_rpt']);
        }
      }
      // Prevent username change
      if(isset($data['username'])) {
        unset($data['username']);
      }
      // Remove password check
      if(isset($data['password_rpt'])) {
        unset($data['password_rpt']);
      }

      // Do not change remote pass if input is empty
      if(isset($data['remote_pass']) && $data['remote_pass']==="") {
        unset($data['remote_pass']);
      }
      $user = $users->patchEntity($user, $data);

      if($valid) {
        if ($users->save($user)) {
            $this->Flash->success(__('Tus datos de usuario han sido actualizados.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('No es posible actualizar tus datos de usuario.'));
      } else {
        $this->Flash->success(__('Error: '.$error_msg));
      }
    }

    $user->password = "";
    $user->remote_pass = "";

    $this->set('user', $user);
  }

  public function devices() {
    $user_id = $this->Auth->user('id');;

    $devices = TableRegistry::getTableLocator()->get('Devices');
    $devices->belongsTo('DeviceFeatures');
    $deviceFeatures = TableRegistry::getTableLocator()->get('DeviceFeatures');

    $user_devices = $this->Paginator->paginate($devices->find()
                          ->select(['id','user_id','name',
                            'short_address','device_feature_id'])
                          ->select($devices->DeviceFeatures)
                          ->where(['user_id'=>$user_id])
                          ->contain(['DeviceFeatures'])
                    );

    $this->set(compact('user_devices'));
  }

  public function addDevice() {
    $devices = TableRegistry::getTableLocator()->get('Devices');
    $devices->belongsTo('DeviceFeatures');
    $device_features = $devices->DeviceFeatures->find('list');
    $device = $devices->newEntity();
    if ($this->request->is('post')) {
      $device = $devices->patchEntity($device, $this->request->getData());

      $device->user_id = $this->Auth->user('id');

      if ($devices->save($device)) {
        $this->Flash->success(__('Tu dispositivo ha sido guardado.'));
        return $this->redirect(['action' => 'devices']);
      }
      $this->Flash->error(__('No es posible agregar tu dispositivo.'));
    }
    $this->set('device_features', $device_features);
    $this->set('device', $device);
  }

  public function editDevice($id) {
    $devices = TableRegistry::getTableLocator()->get('Devices');
    $devices->belongsTo('DeviceFeatures');
    $device_features = $devices->DeviceFeatures->find('list');
    $device = $devices->findById($id)->firstOrFail();

    // Prevent edit of devices not belonging to the current user
    if($device->user_id  != $this->Auth->user('id')) {
      $this->Flash->error(__('Operación no autorizada.'));
      return $this->redirect(['action' => 'devices']);
    }

    if ($this->request->is(['post','put'])) {
      $device = $devices->patchEntity($device, $this->request->getData());

      if ($devices->save($device)) {
        $this->Flash->success(__('Tu dispositivo ha sido actualizado.'));
        return $this->redirect(['action' => 'devices']);
      }
      $this->Flash->error(__('No es posible actualizar tu dispositivo.'));
    }
    $this->set('device_features', $device_features);
    $this->set('device', $device);
  }

  public function deleteDevice($id) {
    $devices = TableRegistry::getTableLocator()->get('Devices');
    $device = $devices->findById($id)->firstOrFail();

    // Prevent edit of devices not belonging to the current user
    if($device->user_id  != $this->Auth->user('id')) {
      $this->Flash->error(__('Operación no autorizada.'));
      return $this->redirect(['action' => 'devices']);
    }

    if($devices->delete($device)) {
      $this->Flash->success(__('El dispositivo fue eliminado.', $device->name));
      return $this->redirect(['action' => 'devices']);
    } else {
      $this->Flash->success(__('No es posible eliminar el dispositivo.', $device->name));
      return $this->redirect(['action' => 'devices']);
    }
  }
  public function events() {
    $user_id = $this->Auth->user('id');
    $events =  TableRegistry::getTableLocator()->get('Events');
    $events->belongsTo('Devices');
    $events->hasMany('EventInfos');
    $user_events = $this->Paginator->paginate(
        $events->find()
               ->contain(['Devices','EventInfos'])
               ->where(['Devices.user_id' => $user_id])
      );
    $this->set(compact('user_events'));
  }

  public function remotes() {
    $user_id = $this->Auth->user('id');
    $remotes = TableRegistry::getTableLocator()->get('RemoteIds');
    $pending = $remotes->find()->where(['user_id'=>$user_id,'card_id'=>'-1'])->first();
    $this->set('pending', $pending);
    $user_remotes = $remotes->findByUserId($user_id);
    $this->set('user_remotes', $user_remotes);
  }

  public function syncRemotes() {

    $user_id = $this->Auth->user('id');
    $users = TableRegistry::getTableLocator()->get('Users');
    $remotes = TableRegistry::getTableLocator()->get('RemoteIds');
    $user = $users->findById($user_id)->firstOrFail();

    $data = array('token'=>$user['remote_pass']);

    $data_json = json_encode($data);

    $ch = curl_init($user->remote_url.'/get-users');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: '.strlen($data_json)
    ));

    $remotes_json_plain = curl_exec($ch);

    $remote_users = json_decode($remotes_json_plain);

    $update_fail = False;

    foreach($remote_users as $i=>$user) {
      $remote_in_db = $remotes->find()
                ->where(['user_id'=>$user_id, 'remote_id'=>$user->id]); 
      $remote = $remote_in_db->first();

      if($remote) {
        $remote->name = $user->name;
        $remote->remote_id = $user->id;
        $remote->remote_class = $user->class;
      } else {
        $remote = $remotes->newEntity();
        $remote->name = $user->name;
        $remote->remote_id = $user->id;
        $remote->remote_class = $user->class;
      }
      $remote->user_id = $user_id;
      if(!$remotes->save($remote)) {
        $update_fail |= True;
      }
    }
    if($update_fail) {
      $this->Flash->error('La sincronización falló con uno o varios usuarios remotos');
    } else {
      $this->Flash->success('La sincronización se completó exitosamente');
    }
    return $this->redirect(['action'=>'remotes']);
  }

  public function deleteRemote($id) {
    $remotes = TableRegistry::getTableLocator()->get('RemoteIds');
    $remote = $remotes->findById($id)->firstOrFail();

    // Prevent edit of devices not belonging to the current user
    if($remote->user_id  != $this->Auth->user('id')) {
      $this->Flash->error(__('Operación no autorizada.'));
      return $this->redirect(['action' => 'remotes']);
    }

    if($remotes->delete($remote)) {
      $this->Flash->success(__('El usuario remoto fue eliminado.', $remote->name));
      return $this->redirect(['action' => 'remotes']);
    } else {
      $this->Flash->success(__('No es posible eliminar el usuario remoto.', $remote->name));
      return $this->redirect(['action' => 'remotes']);
    }
 
  }

  public function setCard($remote_id) {
    $user_id = $this->Auth->user('id');
    $users = TableRegistry::getTableLocator()->get('Users');
    $remotes = TableRegistry::getTableLocator()->get('RemoteIds');

    // TODO: Check if there is another remote waiting for card. There should be only one.

    $remote = $remotes->find()
              ->where(['user_id'=>$user_id, 'id'=>$remote_id])
              ->firstOrFail();
    $remote->card_id = '-1'; // We use -1 to indicate that the card should be
                           // set
    if($remotes->save($remote)) {
      $this->Flash->success(__('El remoto está listo para inicializarse'));
    } else {
      $this->Flash->error(__('Hubo un error al establecer la inicialización'));
    }
    return $this->redirect(['action'=>'remotes']);
  }

  // The client pools this to see if there is remaining cards to set.
  public function pendingToSet() {
    $user_id = $this->Auth->user('id');
    $remotes = TableRegistry::getTableLocator()->get('RemoteIds');
    $response = array('pending' => False);

    $remote = $remotes->find()
              ->where(['user_id'=>$user_id,'card_id'=>'-1'])
              ->first();
    
    if($remote) {
      $response['pending'] = True;
      $response['id'] = $remote->id;
    } else {
      $response['pending'] = False;
    }

    $data_json = json_encode($response);

    $this->response->type('json');
    $this->response->body($data_json);
    return $this->response;
  }

  public function lunchCheck(){
    $user_id = $this->Auth->user('id');
    $remotes = TableRegistry::getTableLocator()->get('RemoteIds');
    $response = array('pending' => False);

    $remote = $remotes->find()
              ->where(['user_id'=>$user_id,'interactive'=>'1'])
              ->first();

    if($remote) {
      $response['pending'] = True;
      $response['id'] = $remote->id;
      $remote->interactive = False;
      $remotes->save($remote);
    } else {
      $response['pending'] = False;
    }

    $data_json = json_encode($response);

    $this->response->type('json');
    $this->response->body($data_json);
    return $this->response;
  }

  public function lunch($id = null) {
    $user_id = $this->Auth->user('id');
    $remotes = TableRegistry::getTableLocator()->get('RemoteIds');
    $response = array('pending' => False);

    $remote = $remotes->find()
              ->where(['user_id'=>$user_id,'interactive'=>'1'])
              ->first();

    if($id !== null) {
      $remote = $remotes->find()->where(['id'=>$id])->first();
    }

    if($remote) {
      $remote->lunch2 = True;

      $remotes->save($remote);

      $this->set('remote',$remote);
    }
  }

  public function isAuthorized($user) {
    return true;
  }
}
