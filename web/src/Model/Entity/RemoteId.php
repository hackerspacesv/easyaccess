<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RemoteId Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $remote_id
 * @property int $remote_class
 * @property int $card_id
 * @property string $name
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Remote $remote
 * @property \App\Model\Entity\Card $card
 */
class RemoteId extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'remote_id' => true,
        'remote_class' => true,
        'card_id' => true,
        'name' => true,
        'user' => true,
        'remote' => true,
        'card' => true
    ];
}
