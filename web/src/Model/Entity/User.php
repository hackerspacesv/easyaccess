<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $remote_url
 * @property string $remote_user
 * @property string $remote_pass
 * @property string $panid
 * @property string $short_address
 * @property bool $enabled
 *
 * @property \App\Model\Entity\Device[] $devices
 * @property \App\Model\Entity\RemoteId[] $remote_ids
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'remote_url' => true,
        'remote_user' => true,
        'remote_pass' => true,
        'panid' => true,
        'short_address' => true,
        'enabled' => true,
        'devices' => true,
        'remote_ids' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password', // Hash hidden from output
        'remote_pass' // Remote API pass hidden from clients
    ];


    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }
}
