<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Operaciones') ?></li>
        <li><?= $this->Html->link(__('Dispositivos'), ['action' => 'devices']) ?></li>
        <li><?= $this->Html->link(__('Configuración'), ['action' => 'config']) ?></li>
        <li><?= $this->Html->link(__('Eventos'), ['action' => 'events']) ?></li>
        <li><?= $this->Html->link(__('Usuarios remotos'), ['action' => 'remotes']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
  <h2>¡Bienvenido!</h2>
  <p>Este es el panel de administración de <strong>EasyAccess</strong>, la solución de asistencia
  desarrollada por el Hackerspace San Salvador para el X Encuentro Centroaméricano
  de Sofware Libre.</p>
  <h3>Guía rápida</h3>
  <ol>
    <li>Ve a <?= $this->Html->link('Dispositivos', ['action' => 'devices']) ?> y agrega los
    lectores con sus funciones correspondientes.</li>
    <li>Utiliza el script <i><strong>configure_reader.py</strong></i> para cargar la
    configuración al lector.</li>
    <li>Ve a <?= $this->Html->link(__('Configuración'), ['action' => 'config']) ?> y agrega
    la configuración del punto de conexión de API remoto.</li>
    <li>Ve a <?= $this->Html->link(__('Usuarios remotos'), ['action' => 'remotes']) ?> y has
    clic en "Sincronizar datos" para actualizar la información de usuarios.</li>
    <li>La información de la actividad del sistema estará disponible en 
    <?= $this->Html->link(__('Eventos'), ['action' => 'events']) ?></li>
  </ol>
</div>
