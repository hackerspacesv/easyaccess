<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Operaciones') ?></li>
        <li><?= $this->Html->link(__('Dispositivos'), ['action' => 'devices']) ?></li>
        <li><?= $this->Html->link(__('Configuración'), ['action' => 'config']) ?></li>
        <li><?= $this->Html->link(__('Eventos'), ['action' => 'events']) ?></li>
        <li><?= $this->Html->link(__('Usuarios remotos'), ['action' => 'remotes']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
  <h2>Configuración para el usuario "<i><?php echo $user->username ?><i>"</h2>
  <?php
      echo $this->Form->create($user);
      echo $this->Form->control('password',['label' => 'Clave']);
      echo $this->Form->control('password_rpt',['type'=>'password', 'label' => 'Repetir Clave']);
      echo $this->Form->control('panid',['label' => 'Dirección PAN']);
      echo $this->Form->control('short_address',['label' => 'Dirección nodo central']);
      echo $this->Form->control('remote_url',['label' => 'URL API remota']);
      echo $this->Form->control('remote_user',['label' => 'Usuario API']);
      echo $this->Form->control('remote_pass',['type'=>'password','label' => 'Clave/Token API']);
  ?>
  <strong>Importante:</strong> Al hacer click en "Habilitar" las configuraciones de los demas
  usuarios del sistema serán deshabilitadas ya que la plataforma solo tiene
  soporte para un usuario activo a al vez.<br />.<br />
  <?php
      echo $this->Form->control('enabled',['label'=>'Habilitado']);
      echo $this->Form->button(__('Guardar configuración'));
      echo $this->Form->end();
  ?>
</div>