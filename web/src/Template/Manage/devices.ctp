<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Operaciones') ?></li>
        <li><?= $this->Html->link(__('Dispositivos'), ['action' => 'devices']) ?></li>
        <li><?= $this->Html->link(__('Configuración'), ['action' => 'config']) ?></li>
        <li><?= $this->Html->link(__('Eventos'), ['action' => 'events']) ?></li>
        <li><?= $this->Html->link(__('Usuarios remotos'), ['action' => 'remotes']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
  <h2>Mis dispositivos</h2>
  <table>
    <tr>
      <th>Id</th>
      <th>Función</th>
      <th>Nombre</th>
      <th>Dirección</th>
      <th>Operaciones</th>
    </tr>
    <?php foreach ($user_devices as $device) { ?>
    <tr>
      <td><?php echo $device->id ?></td>
      <td><?php echo (isset($device->device_feature)) ? $device->device_feature->name : '' ?>
      <td><?php echo $device->name ?></td>
      <td>0x<?php echo $device->short_address ?></td>
      <td><?php echo $this->Html->link('Editar', ['action' => 'editDevice',$device->id]); ?> |
          <?php echo $this->Html->link('Borrar', ['action' => 'deleteDevice',$device->id]); ?></td>
    </tr>
    <?php } ?>
  </table>
  <?php echo $this->Html->link(
    __('Agregar nuevo dispositivo'),
    'manage/add-device',
    ['class' => 'button']
); ?>
</div>
