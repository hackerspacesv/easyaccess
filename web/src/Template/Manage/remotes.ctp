<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Operaciones') ?></li>
        <li><?= $this->Html->link(__('Dispositivos'), ['action' => 'devices']) ?></li>
        <li><?= $this->Html->link(__('Configuración'), ['action' => 'config']) ?></li>
        <li><?= $this->Html->link(__('Eventos'), ['action' => 'events']) ?></li>
        <li><?= $this->Html->link(__('Usuarios remotos'), ['action' => 'remotes']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
  <h2>Usuarios remotos</h2>
  <table>
    <tr>
      <th>Id</th>
      <th>Id remoto</th>
      <th>Clase remota</th>
      <th>Nombre</th>
      <th>Tarjeta asociada</th>
      <th>operaciones</th>
    </tr>
    <?php foreach ($user_remotes as $remote) { ?>
    <tr>
      <td><?php echo $remote->id ?></td>
      <td><?php echo $remote->remote_id ?></td>
      <td><?php echo $remote->remote_class ?></td>
      <td><?php echo $remote->name ?></td>
      <td><?php echo $remote->card_id ?></td>
      <td><?php echo $this->Html->link(__('Asociar'), ['action'=>'setCard',$remote->id]) ?> | <?php echo $this->Html->link(__('Borrar'), ['action'=>'deleteRemote',$remote->id]) ?></td>
    </tr>
    <?php } ?>
  </table>
  <?php echo $this->Html->link(
    __('Sincronizar'),
    'manage/sync-remotes',
    ['class' => 'button']
); ?>
</div>
<?php if($pending) { ?>
<script type="text/javascript">
function checkPending() {
  $.ajax({url: "/manage/pending-to-set", success:
    function(result) {
      if(result.pending) {
        window.setTimeout(checkPending, 500);
      } else {
        location.reload();
      }
    }
  });
};

$(function() {
  checkPending();  
});
</script>
<?php } ?>
