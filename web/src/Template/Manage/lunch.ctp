<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Operaciones') ?></li>
        <li><?= $this->Html->link(__('Dispositivos'), ['action' => 'devices']) ?></li>
        <li><?= $this->Html->link(__('Configuración'), ['action' => 'config']) ?></li>
        <li><?= $this->Html->link(__('Eventos'), ['action' => 'events']) ?></li>
        <li><?= $this->Html->link(__('Usuarios remotos'), ['action' => 'remotes']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
  <h2>Info Almuerzo</h2>
<?php if(isset($remote)) { ?>
  <p> El participante <?php echo $remote->name ?></p>
  <?php if($remote->remote_class == 1) { ?>
  <p>Tiene derecho a almuerzo</p>
  <?php } else { ?>
  <p>No tiene derecho a almuerzo</p>
  <?php } ?> 
  <p><?php if($remote->lunch1 == 1) { ?> Ya recibio almuerzo jueves <?php } else { ?> No tiene almuerzo jueves <?php } ?></p>
  <p><?php if($remote->lunch2 == 1) { ?> Ya recibio almuerzo viernes <?php } else { ?> No tiene almuerzo viernes <?php } ?></p>
<?php } ?>
</div>
<script type="text/javascript">
function checkPending() {
  $.ajax({url: "/manage/lunch-check", success:
    function(result) {
      if(!result.pending) {
        window.setTimeout(checkPending, 500);
      } else {
        window.location = "/manage/lunch/"+result.id;
      }
    }
  });
};

$(function() {
  checkPending();  
});
</script>
