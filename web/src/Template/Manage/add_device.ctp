<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Operaciones') ?></li>
        <li><?= $this->Html->link(__('Dispositivos'), ['action' => 'devices']) ?></li>
        <li><?= $this->Html->link(__('Configuración'), ['action' => 'config']) ?></li>
        <li><?= $this->Html->link(__('Eventos'), ['action' => 'events']) ?></li>
        <li><?= $this->Html->link(__('Usuarios remotos'), ['action' => 'remotes']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
  <h2>Agregar nuevo dispositivo</h2>
  <?php
      echo $this->Form->create($device);
      echo $this->Form->control('user_id', ['type' => 'hidden', 'value' => 1]);
      echo $this->Form->control('name');
      echo $this->Form->control('short_address');
      echo $this->Form->control('device_feature_id',['options'=>$device_features]);
      echo $this->Form->button(__('Guardar dispositivo'));
      echo $this->Form->end();
  ?>
</div>
