<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Operaciones') ?></li>
        <li><?= $this->Html->link(__('Dispositivos'), ['action' => 'devices']) ?></li>
        <li><?= $this->Html->link(__('Configuración'), ['action' => 'config']) ?></li>
        <li><?= $this->Html->link(__('Eventos'), ['action' => 'events']) ?></li>
        <li><?= $this->Html->link(__('Usuarios remotos'), ['action' => 'remotes']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
  <h2>Eventos</h2>
  <table>
    <tr>
      <th>Id</th>
      <th>Dispositivo</th>
      <th>Datos (JSON)</th>
    </tr>
    <?php foreach ($user_events as $event) { ?>
    <tr>
      <td><?php echo $event->id ?></td>
      <td><?php echo $event->device->name ?></td>
      <td><?php echo json_encode($event->event_infos) ?></td>
    </tr>
    <?php } ?>
  </table>
</div>
