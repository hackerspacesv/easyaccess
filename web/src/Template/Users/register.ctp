<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Registro') ?></legend>
        <?php
            echo $this->Form->control('username',['label'=>__('Usuario')]);
            echo $this->Form->control('password',['label'=>__('Clave')]);
            echo $this->Form->control('password_rpt',['type'=>'password','label'=>__('Repetir clave')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar información')) ?>
    <?= $this->Form->end() ?>
</div>
