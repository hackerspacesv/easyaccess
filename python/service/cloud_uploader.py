import json
import requests
import sqlite3
import time
import datetime
import glob
import sys
from getpass import getpass
import os

# Check if the number is an integer
def is_int(number):
    result = False
    try:
        int(number)
        result = True
    except Exception as e:
        result = False
    return result

# Waits on the serial port for a response or 
def wait_for(port, text, timeout=5000):
    print("Waiting for answer...")
    found = False
    last_read = time.time()
    string = ""
    port.flush()
    while True:
        if time.time()-last_read>timeout:
            break
        string += port.read(1)
        to_read = port.in_waiting
        if to_read > 0:
            string += port.read(to_read)
            if string.find('CMD_OK') != -1:
                print(string)
                found = True
                break
    port.reset_input_buffer()
    if not found:
        raise Exception('String not found')

if __name__ == '__main__':
    go = True

    conn = sqlite3.connect('/home/pi/easyaccess/web/easy_access.db')

    cur = conn.cursor()

    cur.execute("SELECT remote_url,remote_pass,remote_user,id FROM users WHERE enabled = 1")

    config = cur.fetchone()

    if config is None:
      print("No valid configuration was found.")
      print("Please enable one user profile on the admin panel.")
      exit()


    while go:
        # Load server address
        base_url = config[0]
        if base_url == "":
            go = False
            continue

        # Load token
        token = config[1]
        if token == "":
            go = False
            continue

        print("Looking for new events")
        result = cur.execute('SELECT events.id,device_features.feature_key,devices.id,devices.name,events.timestamp FROM events INNER JOIN devices ON events.device_id = devices.id INNER JOIN device_features ON devices.device_feature_id = device_features.id  WHERE device_features.feature_key=0 AND devices.user_id = ?', (config[3],))

        new_events = []

        for row in result:
          cur2 = conn.cursor()
          cur2.execute('SELECT key,value FROM event_infos WHERE event_id = ? and key = ?',(row[0],'remote_id'))
          evt_data = cur2.fetchone()
          data = {'event_id': row[0], 'event_type_id': row[1], 'reader_id': row[2],'reader_name': row[3],'timestamp':datetime.datetime.fromtimestamp(int(row[4])).strftime('%Y-%m-%dT%H:%M:%SZ'), 'data': ({'user_id': int(evt_data[1])},)}
          new_events.append(data)

        if len(new_events)>0:
          request_data = {'token': token, 'data': new_events}
          print(json.dumps(request_data))
          ##r = requests.post(base_url+'/post-card-touch', json = request_data)

          #print(r.text)

        print("Looking for sharepoint")
        result = cur.execute('SELECT events.id,device_features.feature_key,devices.id,devices.name,events.timestamp FROM events INNER JOIN devices ON events.device_id = devices.id INNER JOIN device_features ON devices.device_feature_id = device_features.id  WHERE device_features.feature_key=1 AND devices.user_id = ?', (config[3],))

        new_events = []

        for row in result:
          users_data = []
          cur2 = conn.cursor()
          cur2.execute('SELECT key,value FROM event_infos WHERE event_id = ? and key = ?',(row[0],'remote_id_1'))
          evt_data = cur2.fetchone()
          users_data.append({'user_id': evt_data[1]})
          cur2.execute('SELECT key,value FROM event_infos WHERE event_id = ? and key = ?',(row[0],'remote_id_2'))
          evt_data = cur2.fetchone()
          users_data.append({'user_id': evt_data[1]})
          data = {'event_id': row[0], 'event_type_id': row[1], 'reader_id': row[2],'reader_name': row[3],'timestamp':datetime.datetime.fromtimestamp(int(row[4])).strftime('%Y-%m-%dT%H:%M:%SZ'), 'data': users_data}
          new_events.append(data)

        if len(new_events)>0:
          request_data = {'token': token, 'data': new_events}
          print(json.dumps(request_data))
          try:
            r = requests.post(base_url+'/post-share-info', json = request_data)
            print("RESPONSE")
            print(r.text)
          except:
            print("Error, check connection")
        
        print("Trying again")
        time.sleep(5)
