#!/usr/bin/python
import signal
import sys
import RPi.GPIO as gpio
import sqlite3
import time
import logging

sys.path.append('/home/pi/easyaccess/modules/I32CTT/python/')

from phy.at86rf233_rpi.driver import driver_at86rf233
from mac.ieee802154.driver import driver_ieee802154
from i32ctt.driver import driver_i32ctt

conn = None
cur = None
user_id = None

logging.basicConfig(filename='/home/pi/easyaccess/python/daemon.log',level=logging.DEBUG)

run = False
phy = None
mac = None
i32ctt = None

def service_setup():
  global run
  run = True

def _hx(byte):
  return "{:02X}".format(byte)

def hex_to_int(ba): # TODO: Make it pretty:
  hex_string = "0x"+_hx(ba[3])+_hx(ba[2])+_hx(ba[1])+_hx(ba[0]); 
  intval = int(hex_string, 16)
  logging.info(hex_string)
  logging.info(str(intval))
  return intval

def service_main():
  global run, instance, phy, mac, i32ctt, run, conn, cur, user_id
  logging.basicConfig(filename='/home/pi/easyaccess/python/daemon.log',level=logging.DEBUG)
  logging.info("Trying to get packets"+str(run))

  conn = sqlite3.connect('/home/pi/easyaccess/web/easy_access.db');

  cur = conn.cursor()

  logging.info("Ready to start")
  phy = driver_at86rf233(gpio)
  logging.info("Physical interface started")
  mac = driver_ieee802154(phy)
  logging.info("MAC driver initialized")
  i32ctt = driver_i32ctt(mac)
  logging.info("I32CTT initialized")

  logging.info("Getting configuration from database")

  cur.execute("SELECT panid,short_address,id FROM users WHERE enabled = 1")

  config = cur.fetchone()

  logging.info(str(config))

  if config is None:
    logging.info("No valid configuration was found.")
    logging.info("Please enable one user profile on the admin panel.")
    gpio.cleanup()
    exit()

  pan_id = int("0x"+config[0], 16)
  short_address = int("0x"+config[1], 16)
  user_id = config[2]

  mac.escr_config_red(canal=26, pan_id=pan_id, dir_corta=short_address)

  run = True
  try:
    while run:
      if mac.hay_paquete():
        logging.info("Hay paquete")
        origen, paquete = mac.recibir_paquete()
        str_address = str( "{:04X}".format(origen))
            
        cur.execute('SELECT * FROM devices WHERE short_address = ?', (str_address,))
      
        device = cur.fetchone()
        #TODO: Check if configured feature match with device action
        if device:
          device_id = device[0]
          logging.info('Device found in System')
          # Check first byte to determine command
          logging.info(str(paquete[0]))
          if paquete[0] == 3: # Write request
            if paquete[1] == 0: # Visita
              logging.info("Recording visit")
              # Get remote id:
              card_id = hex_to_int(paquete[4:8])
              cur.execute('SELECT id,remote_id FROM remote_ids WHERE card_id = ? and user_id = ?', (card_id,user_id))
              remote = cur.fetchone()
              if remote:
                remote_id = remote[0]
                remote_platform_id = remote[1]
                cur.execute('INSERT INTO events(device_id,remote_id,timestamp) VALUES(?,?,?)', (device_id,remote_id,int(time.time())))
                try:
                  conn.commit()
                  event_id = cur.lastrowid
                  cur.execute('INSERT INTO event_infos(event_id,key,value) VALUES(?,?,?)', (event_id,'card_id',card_id))
                  cur.execute('INSERT INTO event_infos(event_id,key,value) VALUES(?,?,?)', (event_id,'remote_id',remote_platform_id))
                  conn.commit()
                except sqlite3.Error as er:
                  logging.info("Cannot store event:".str(er))
              else:
                logging.info("Error, card not found");
            if paquete[1] == 1: # SharePoint
              logging.info("Recording sharepoint event")
              # First check if both cards exists within the system
              if len(paquete)==14:
                # Discard packets of wrong length
                card_1_id = hex_to_int(paquete[4:8])
                card_2_id = hex_to_int(paquete[10:14])
                cur.execute('SELECT COUNT(*) AS cards FROM remote_ids WHERE (card_id = ? OR card_id = ?) AND user_id = ?', (card_1_id, card_2_id, user_id))
                card_count = cur.fetchone()
                if card_count[0] == 2:
                  logging.info("Cards found on system")
                  # Cards found on system
                  cur.execute('INSERT INTO events(device_id,remote_id,timestamp) VALUES(?,(SELECT remote_id FROM remote_ids WHERE card_id = ?),?)',(device_id,card_1_id,int(time.time())))
                  try:
                    conn.commit()
                    event_id = cur.lastrowid
                    cur.execute('INSERT INTO event_infos(event_id,key,value) VALUES(?,?,(SELECT remote_id FROM remote_ids WHERE card_id = ?))',(event_id, 'remote_id_1', card_1_id))
                    cur.execute('INSERT INTO event_infos(event_id,key,value) VALUES(?,?,(SELECT remote_id FROM remote_ids WHERE card_id = ?))',(event_id, 'remote_id_2', card_2_id))
                    conn.commit()
                  except sqlite3.Error as er:
                    logging.info("Cannot store event: "+str(er))
            if paquete[1] == 2: # Init card
              logging.info("Card init evt received")
              cur.execute('SELECT id FROM remote_ids WHERE card_id = -1 and user_id = ?',(user_id,))
              remote = cur.fetchone()
              if remote is not None:
                logging.info("Trying to store card ID");
                # update card id
                remote_id = remote[0]
                card_id = hex_to_int(paquete[4:8])
                logging.info(str(card_id))
                cur.execute('UPDATE remote_ids SET card_id = ? WHERE id = ?',(card_id,remote_id))
                try:
                  conn.commit()
                except sqlite3.Error as er:
                  logging.info("Cannot store card_id:".str(er))
              else:
                logging.info("No card available to init");
            if paquete[1] == 3: # Almuerzo
              logging.info("Lunch request evt received")
              card_id = hex_to_int(paquete[4:8])
              cur.execute('SELECT id,remote_id FROM remote_ids WHERE card_id = ? and user_id = ?', (card_id,user_id))
              remote = cur.fetchone()
              if remote is not None:
                logging.info("Updating remote lunch");
                remote_id = remote[0]
                logging.info(str(card_id))
                cur.execute('UPDATE remote_ids SET interactive = 1 WHERE card_id = ? AND user_id = ?', (card_id, user_id))
                try:
                  conn.commit()
                except sqlite3.Error as er:
                  logging.info("Cannot update lunch".str(er))
              else:
                logging.info("Card not in system")

        logging.info("Paquete de {:04X}".format(origen))
        logging.info("Recibido: ")
        for i in paquete:
          logging.info("{:02X}".format(i))

  except Exception as e:
    logging.info("Error: "+str(e))  
  finally:
    gpio.cleanup()

def service_cleanup():
  global run
  run = False

def service_reload():
  logging.info("Reloading config...")
