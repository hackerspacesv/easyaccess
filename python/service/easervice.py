import os
import grp
import signal
import daemon
import lockfile
import eagateway

from eagateway import (
  service_setup,
  service_main,
  service_cleanup,
  service_reload
  )

context = daemon.DaemonContext(
  working_directory='/var/lib/eagateway',
  umask=0o002,
  pidfile=lockfile.FileLock('/var/run/eagateway.pid')
  )

context.signal_map = {
  signal.SIGTERM: service_cleanup,
  signal.SIGHUP: 'terminate',
  signal.SIGUSR1: service_reload
  }

eagateway_gid = grp.getgrnam('pi').gr_gid
context.gid = eagateway_gid

#db_file = open('ea_database.db', 'w')

context.files_preserve = [] # [db_file]

service_setup()
with context:
  service_main()
