#!/usr/bin/python
import json
import requests
import serial
import time
import glob
import sys
from getpass import getpass
import os

# Check if the number is an integer
def is_int(number):
    result = False
    try:
        int(number)
        result = True
    except Exception as e:
        result = False
    return result

# Waits on the serial port for a response or 
def wait_for(port, text, timeout=5000):
    print("Waiting for answer...")
    found = False
    last_read = time.time()
    string = ""
    port.flush()
    while True:
        if time.time()-last_read>timeout:
            break
        string += port.read(1)
        to_read = port.in_waiting
        if to_read > 0:
            string += port.read(to_read)
            if string.find('CMD_OK') != -1:
                print(string)
                found = True
                break
    port.reset_input_buffer()
    if not found:
        raise Exception('String not found')

def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

if __name__ == '__main__':
    go = True

    while go:
        # Ask for server address
        server_addr = raw_input('Address: ')
        if server_addr == "":
            go = False
            continue

        # Ask for username
        username = raw_input('Username: ')
        if username == "":
            go = False
            continue

        # Ask for password
        os.system('stty -echo')
        password = raw_input('Password: ')
        os.system('stty echo')
        if password == "":
            go = False
            continue

        base_url = 'http://'+server_addr

        # Get session token
        r = requests.post(base_url+'/users/login', data = {'username':username, 'password':password })

        if r.status_code == 200 and len(r.history) == 1 and 'CAKEPHP' in r.history[0].cookies:
            print("\r\nSuccessful authentication!")
        else:
            go = False
            continue

        # Get user network settings
        config_r = requests.get(base_url+'/api/get-config', cookies=r.history[0].cookies)

        if config_r.status_code != 200:
            go = False
            continue

        # Parse JSON response
        config = json.loads(config_r.text)

        print("Current network configuration:")
        print("PAN ID: 0x"+config['panid'])
        print("Gateway short address: 0x"+config['short_address'])

        # Get user devices
        devices_r = requests.get(base_url+'/api/get-devices', cookies=r.history[0].cookies)

        if devices_r.status_code != 200:
            go = False
            continue

        devices = json.loads(devices_r.text)
	ports = serial_ports()

	configure_device = True
	while configure_device:
	    print("\r\nDevices available:\r\n")
            i = 0
            for device in devices:
                print(str(i)+"-"+str(device['name']+'(0x'+device['short_address']+')'))
                i = i+1

            current_device = raw_input('\r\nPlease choose one (Leave empty to exit):')
	    if current_device == "" or (is_int(current_device) and (int(current_device)<0 or int(current_device)>=len(devices))):
                configure_device = False
                continue

            print("\r\nPorts available:")
	    j = 0
            for port in ports:
                print(str(j)+"-"+port)
                j = j+1

	    current_port = raw_input('\r\nPlease choose one (Leave empty to exit):')
            if current_port == "" or (is_int(current_port) and (int(current_device)<0 or int(current_device)>=len(devices))):
                configure_device = False
                continue

            print("Using: "+ports[int(current_port)])

	    print("\r\nTrying to configure device...")
            port = serial.Serial(ports[int(current_port)],timeout=1)
            try:
                port.isOpen()
            except IOError:
                port.close()
                port.open()

	    d = int(current_device)
            try:
                command = ("set_pan_id:"+config['panid']+"\r\n").encode()
                print("Sending..."+command)
                port.write(command)
		wait_for(port,"CMD_OK")
                print("[DONE]")
                command = ("set_short_addr:"+devices[d]['short_address']+"\r\n").encode()
                print("Sending..."+command)
                port.write(command)
                wait_for(port,"CMD_OK")
                print("[DONE]")
                command = ("set_gateway_addr:"+config['short_address']+"\r\n").encode()
                print("Sending..."+command)
                port.write(command)
                wait_for(port,"CMD_OK")
                print("[DONE]")
                command = ("set_mode:"+str(devices[d]['device_feature']['feature_key'])+"\r\n").encode()
                print("Sending..."+command)
                port.write(command)
                wait_for(port,"CMD_OK")
                print("[DONE]")

                print('Configuration of device "'+devices[d]['name']+'" finished.\r\n')
                port.close()
            except Exception as e:
                print("Error writting configuration to device: "+str(e))

        go = False

print("Saliendo...")
