CREATE TABLE users (
    id          INTEGER       PRIMARY KEY AUTOINCREMENT,
    username    CHAR (20)     UNIQUE
                              NOT NULL,
    password    VARCHAR (64),
    remote_url  VARCHAR (255),
    remote_user CHAR (20),
    remote_pass CHAR (20) 
);

CREATE TABLE remote_ids (
    id        INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id   INTEGER NOT NULL,
    remote_id INTEGER,
    card_id   INTEGER,
    UNIQUE (
        user_id,
        remote_id
    )
);

CREATE TABLE devices (
    id            INTEGER   PRIMARY KEY AUTOINCREMENT
                            UNIQUE
                            NOT NULL,
    user_id       INTEGER   NOT NULL,
    name          CHAR (20) UNIQUE
                            NOT NULL,
    short_address CHAR (4)  UNIQUE
                            NOT NULL
);

CREATE TABLE device_features (
    id          INTEGER PRIMARY KEY
                        UNIQUE,
    device_id   INTEGER NOT NULL,
    feature_key INTEGER NOT NULL,
    name                NOT NULL,
    UNIQUE (
        device_id,
        feature_key
    )
);

CREATE TABLE events (
    id                INTEGER PRIMARY KEY AUTOINCREMENT
                              UNIQUE,
    device_id         INTEGER NOT NULL,
    remote_id         INTEGER NOT NULL,
    device_feature_id INTEGER NOT NULL,
    timestamp         BIGINT  NOT NULL,
    UNIQUE (
        device_id,
        remote_id,
        device_feature_id,
        timestamp
    )
);

CREATE TABLE event_infos (
    id       INTEGER   PRIMARY KEY AUTOINCREMENT
                       UNIQUE,
    event_id INTEGER   NOT NULL,
    [key]    CHAR (20) NOT NULL,
    value    TEXT      NOT NULL,
    UNIQUE (
        event_id,
        [key]
    )
);
