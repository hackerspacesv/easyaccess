# Documentación de API

Este archivo contiene la documentación de API utilizada en **EasyAccess**
para sincronización de usuarios en el controlador local IoT.

## Sincronización de Usuarios

Inicialmente EasyAccess asume que existe una lista remota de personas a
rastrear con tarjetas NFC. A nivel local, el concentrador IoT almacena
la configuración del sistema remoto en el perfil local de usuario. De 
esta manera se pueden guardar en el concentrador local múltiples 
configuraciones y estas pueden ser habilitadas según las
necesidades del usuario.

EasyAccess asume que el sistema remoto almacena al menos la
siguiente información del usuario:

|   id   |   name  |class|
|--------|---------|-----|
|`bigint`|`varchar`|`int`|

Donde:
* *id*: Identificador único de usuario en la plataforma remota.
* *name*: Nombre "para humanos".
* *class*: Identifica de forma única grupos de personas a rastrear
(1: Con almuerzo y 2: Sin almuerzo).

Todas las funciones del lado del servidor deberán aceptar JSON en
el cuerpo de la petición y deberán ofrecer respuestas en formato JSON.

### Petición /get-users

**Descripción:** Obtiene la lista de personas en el servidor remoto.

**Métodos aceptados:** GET

**Sintaxis**:
> <URL_BASE>/get-users[/<id_inicial>[/<id_final>]]

**Parámetros:**
* *id_inicial*: Al definir este parámetro el servidor remoto deberá
responder con los IDs iguales o superiores al ID especificado.
* *id_final*: Al definir este parámetro el servidor remoto incluirá 
este ID como último registro en la respuesta. Si se desea especificar 
este parámetro es obligatorio especificar el parámetro *id_inicial*. 
Si este parámetro es inferior a *id_inicial* el servidor deberá 
responder con un error 400 HTTP (Bad Request). El servidor deberá 
aceptar *id_finales* superiores a los almacenados en la base de 
datos sin generar errores, si esto sucede la respuesta deberá 
contener hasta el último registro disponible en la base de datos.

**Ejemplo:**

Solicitud:
> POST http://localhost/api/get-users


Cuerpo del mensaje:
```
{
    "token": abcdef123456789
}
```

Respuesta:
```
[
   {
     "id": 1,
     "class": 1,
     "name": "Mario G\u00f3mez"
   },
   {
     "id": 2,
     "class": 2,
     "name": "Carlos Hern\u00e1ndez"
   },
   {
     "id": 3,
     "class": 1,
     "name": "Juan Juarez"
   }
 ]
```

## Notificación de eventos

EasyAccess utiliza el método POST para notificar al servidor
remoto de la ocurrencia de eventos. Cada evento envía al
servidor la siguiente información:

|event_id|event_type_id|reader_id|reader_name|timestamp|  data |
|--------|-------------|---------|-----------|---------|-------|
|`bigint`|`int`        | `int`   | `varchar` |`varchar`|`array`|

Donde:
* *event_id*: Identificador único de evento en el concentrador IoT.
* *event_type_id*: Tipo de evento que puede ser:
  * *0*: Registro de visita.
  * *1*: Registro de sharepoint.
* *reader_id*: Identifica de forma única el lector que genera el
evento en el concentrador IoT.
* *reader_name*: Nombre del lector "para humanos". 
* *timestamp*: Hora y fecha en que ocurrió el evento. 

Cada evento tiene asociado la data de las tarjetas en contacto 
durante el evento específico.

**Nota:** A cada petición el sistema remoto debería responder 
con los "ids" de eventos que han sido procesados por el sistema.
De esta manera el concentrador local no volverá a enviarlos 
en peticiones futuras.

## Petición /post-card-touch

**Descripción:** Esta petición empuja toques de tarjeta en los
lectores del sistema.

**Métodos aceptados:** POST

**Sintaxis**:
> <URL_BASE>/post-card-touch

**Parámetros:** Esta función no envía parámetros.

**Ejemplo:**
Solicitud:
> POST http://localhost/api/post-card-touch

Cuerpo del mensaje:
```
{
    "token": abcdef123456789,
    "data": [
       {
         "event_id": 100,
         "event_type_id": 0,
         "reader_id": 1,
         "reader_name": "Hall",
         "timestamp": "2010-06-09T15:20:00Z",
         "data": [
           { "user_id": 1 }
         ]
       }
   ]
}
```
Respuesta:
```
[
   {
     "event_id": 100
   }
]
```

## Petición /post-share-info

**Descripción:** Esta petición notifica que dos
usuarios han utilizado un "Sharepoint" para 
compartir su información de contacto.

**Métodos aceptados:** POST

**Sintaxis**:
> <URL_BASE>/post-share-info

**Parámetros:** Esta función no envía parámetros.

**Ejemplo:**

Solicitud:
> POST http://localhost/api/post-share-info

Cuerpo del mensaje:
```
{
    "token": abcdef123456,
    "data": [
       {
         "event_id": 105,
         "event_type_id": 1,
         "reader_id": 2,
         "reader_name": "Room42",
         "timestamp": "2010-06-09T15:20:00Z"
         "data": [
         { "user_id": 1 },
         { "user_id": 2 }
         ]
       }
    ]
}
```

Respuesta:
```
[
   {
     "event_id": 105
   }
 ]
```
